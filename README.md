# 小红花会员管理系统

#### 介绍
这是一个通用型的会员管理系统，可以自定义会员等级和折扣，会员消费物品或类型

#### 软件背景

原本是想开发一套“美发店专用”的会员管理系统，写着写着要有扩展性，就写成的可自定义的，这套项目属于早期刚学C#的时候写的，代码还有很多可以优化的地方，有想继续完善的朋友可以评论一下。

软件的初期开发我做了大量的业务逻辑分析包括用SQL模仿业务逻辑等等，为了防止忘记业务，就画了个功能的思维导图防止自己忘记某些业务逻辑，实际开发实际差不多用了7天，整套项目也很适合新手练手模仿，欢迎上手哦

#### 技术栈

- C# winform
- sqlserver
- 调用摄像头扫描二维码
- 短信接口
- 以及一堆增删改查

#### 部署说明

只需要导入MemberManagementDB.sql和MemberUserDB.sql，再把DBHelper.cs和frmLogin.cs里面的数据库连接字符串修改一下就可以部署上线，MemberBackgroundManagementSystem由于是临时写的，需要改每一个文件的数据库连接字符串，也可以去掉登录，爱怎么搞怎么搞啦

#### 软件效果图

<img  src="https://img-blog.csdnimg.cn/img_convert/d486f1c5ed085eaf86f656b21053f36c.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/9e0247725fa8f34a80a49af562d29f04.png">

<img src="https://img-blog.csdnimg.cn/img_convert/1d7e7ec1ee4231d3974714d0523ad494.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/63dd0e8942e3497d07ada70e58916e1f.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/210e3104fe392a162cfe116e48252a6a.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/aa47790e70f8ed3c2d5b50de9028b7a1.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/5359de1ba48d68da0a31725e75bb2dbb.png">

<img src="https://img-blog.csdnimg.cn/img_convert/d93115fccb35d7658281bc0d1afc06cb.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/7ab3250935e15e24275cf844d587e82d.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/fa72a557c29bf2d7edd1ca314a53d654.png"/>

<img src="https://img-blog.csdnimg.cn/img_convert/72f8eb9f89b9ba19d1f8fa22b518ac64.png"/>
