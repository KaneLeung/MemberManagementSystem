use MemberManagementDB

--dbo.Balance				余额表
--dbo.ConsumptionPatterns	消费方式表
--dbo.ConsumptionType		消费类型表
--dbo.MembershipGrade		会员等级表
--dbo.MethodPayment			支付方式表
--dbo.Orders				订单记录表
--dbo.UserInfo				用户表
--dbo.Recharge				充值记录表
--dbo.Sms					短信记录表
--dbo.SmsType				短信类型表


--查询用户表
select U.CardNo as 卡号,U.MemberState as 会员状态, U.Name as 姓名
,U.PassWord as 密码,U.Wechat as 微信,U.Phone as 手机号,U.Sex as 性别
,U.Birthday as 生日,M.MembersName as 会员等级,U.Note as 备注
,U.OpenCardDate as 开卡日期,U.DueToTime as 到期时间
from Userinfo as U,MembershipGrade as M
where U.LevelNo = M.LevelNo

--查询订单表
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo


--查询支付方式表
select MethodPaymentNo as 支付方式序号,TypeName as 支付方式名称
from MethodPayment



--查询会员等级表
select LevelNo as 会员等级序号,MembersName as 会员等级名称
,Discount as 折扣
,Enable as 是否启用,DueToTime as 到期时间
from MembershipGrade


--查询消费类型表
select ConsumptionTypeNo as 消费类型序号,TypeName as 消费类型名
,Price as 价格,Enable as 是否启用,DueToTime as 到期时间
from ConsumptionType




--查询消费方式表
select ConsumptionPatternsNo as 消费方式序号,TypeName as 消费方式名
from ConsumptionPatterns

--查询余额表
select CardNo as 卡号,Balance as 余额
from Balance

--查询充值记录表
select R.CardNo as 卡号,R.Name as 姓名,R.Credit as 充值金额,R.ActualMoney as 实收金额,R.CreDate as 充值日期,M.TypeName as 支付方式
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo


select R.CardNo ,R.Name,R.Credit,R.ActualMoney,R.CreDate,M.TypeName
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo and DATEDIFF(day, R.CreDate , '{0}')=0


--查询短信表
select CardNo as 卡号,Name as 姓名,Phone as 手机号,InDate as 发送日期,MessageText as 发送内容,MessageTypeNo as 短信类型
from sms

insert Sms
values ('2462623','林鸿辉','18559440650',GETDATE(),'你好',1)

--查询短信类型表
select MessageTypeNo as 短信类型序号,TypeName as 短信类型名称
from smsType



select MethodPaymentNo,TypeName from MethodPayment


--添加消费方式
insert ConsumptionPatterns
values ('微信'),('手机'),('卡号')

--添加支付方式
insert MethodPayment
values ('微信'),('支付宝'),('会员卡')

--添加消费类型
insert ConsumptionType
values ('理发','25',1,'2020-10-20')
,('洗头','50',1,'2020-10-20')
,('染发','300',1,'2020-10-20')

--添加会员等级
insert MembershipGrade
values('普通会员',200,0.9,'洗发',2,1,'2020-12-12')
,('中级会员',400,0.8,'理发',3,1,'2020-12-12')
,('高级会员',800,0.7,'染发导膜',3,1,'2020-12-12')



select o.CardNo,o.Name,cp.TypeName
,M.TypeName,ct.TypeName
,o.ConsumptionMoney,o.ActualMoney ,o.DateConsumption ,o.Note 
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo  and o.ConsumptionPatternsNo = 3
---------------------------------新建会员------------------------------------------------

--添加用户
insert UserInfo values ('0001',1,'林先生','123123','lhh002','18559440650',1,'2000-02-14',3,'这是特别账号',GETDATE(),'')

--获取套餐金额
select money from MembershipGrade where LevelNo = 3

--添加余额
insert Balance values ('0001',800,3)

--查询会员等级
select MembersName from MembershipGrade

-----------------------------------------------------------------------------------------


select ConsumptionPatternsNo,TypeName from ConsumptionPatterns


---------------------------------会员充值------------------------------------------------




--通过微信查找卡号
select CardNo
from UserInfo
where Wechat = 'lhh002'

--通过手机号查找卡号
select CardNo
from UserInfo
where Phone = '18559440650'

--通过姓名查找卡号
select CardNo
from UserInfo
where Name = '林先生'


--查询卡号，手机号，名称，余额
select U.CardNo,U.Phone,U.Name,B.Balance
from UserInfo as U,Balance as B
where U.CardNo = B.CardNo and U.CardNo = '0001'



--充值通过卡号
insert Recharge values ('0001','林先生',300,250,GETDATE(),1);
update Balance 
set Balance = 300
where CardNo = '0001' 


-----------------------------------------------------------------------------------------


---------------------------------管理会员------------------------------------------------

--显示所有会员信息
select U.CardNo as 卡号,U.MemberState as 会员状态, U.Name as 姓名
,U.PassWord as 密码,U.Wechat as 微信,U.Phone as 手机号,U.Sex as 性别
,U.Birthday as 生日,M.MembersName as 会员等级,B.Balance as 余额,U.Note as 备注
,U.OpenCardDate as 开卡日期,U.DueToTime as 到期时间
from Userinfo as U,MembershipGrade as M ,Balance as B
where U.LevelNo = M.LevelNo and U.CardNo = B.CardNo


select CardNo,MemberState,DueToTime
from UserInfo


select CardNo
from UserInfo
where DueToTime>'2000-01-01' and DATEDIFF(DD,GETDATE(),DueToTime)<=0 and MemberState = 1


update UserInfo  set MemberState = 0 where CardNo = ''






select U.CardNo,U.MemberState, U.Name
,U.PassWord,U.Wechat,U.Phone,U.Sex
,U.Birthday,U.LevelNo,U.Note,U.DueToTime
from Userinfo as U
where U.CardNo =''

--通过微信查找卡号
select CardNo
from UserInfo
where Wechat = 'lhh002'

--通过手机号查找卡号
select CardNo
from UserInfo
where Phone = '18559440650'

--通过姓名查找卡号
select CardNo
from UserInfo
where Name = '林先生'

--删除全部
delete from Balance
delete from Orders
delete from Recharge
delete from UserInfo


--删除会员
delete from Balance where CardNo = '0002';
delete from Orders where CardNo = '0002';
delete from Recharge where CardNo = '0002';
delete from UserInfo where CardNo = '0002'


--会员锁定
update UserInfo
set MemberState = 0
where CardNo = '0001'


--修改会员信息
update UserInfo
set MemberState = 0,Name = 'king',PassWord = '1',Wechat = '',Phone = ''
,Sex = 1,Birthday = '',LevelNo = 1,Note = '',DueToTime = ''
where CardNo = '0001'
-----------------------------------------------------------------------------------------






---------------------------------会员消费------------------------------------------------


--通过微信查找卡号
select CardNo
from UserInfo
where Wechat = 'lhh002'

--通过手机号查找卡号
select CardNo
from UserInfo
where Phone = '18559440650'

--通过姓名查找卡号
select CardNo
from UserInfo
where Name = '林先生'

--查询订单表
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo


--查询消费方式
select ConsumptionTypeNo,TypeName ,Price ,Enable ,DueToTime 
from ConsumptionType
where ConsumptionTypeNo=1

--消费
insert Orders values ('0001','林先生',3,3,3,300,300,GETDATE(),'')
update Balance set Balance = '0' where CardNo = '0001'

--消费记账
insert Orders values ('','林先生','',2,3,300,300,GETDATE(),'')


-----------------------------------------------------------------------------------------




---------------------------------会员消费------------------------------------------------
--显示所有会员信息
select U.CardNo as 卡号,U.MemberState as 会员状态, U.Name as 姓名
,U.PassWord as 密码,U.Wechat as 微信,U.Phone as 手机号,U.Sex as 性别
,U.Birthday as 生日,M.MembersName as 会员等级,B.Balance as 余额,U.Note as 备注
,U.OpenCardDate as 开卡日期,U.DueToTime as 到期时间
from Userinfo as U,MembershipGrade as M ,Balance as B
where U.LevelNo = M.LevelNo and U.CardNo = B.CardNo



--通过开卡日期显示所有会员信息-- 30天内的所有数据
select U.CardNo as 卡号,U.MemberState as 会员状态, U.Name as 姓名
,U.PassWord as 密码,U.Wechat as 微信,U.Phone as 手机号,U.Sex as 性别
,U.Birthday as 生日,M.MembersName as 会员等级,B.Balance as 余额,U.Note as 备注
,U.OpenCardDate as 开卡日期,U.DueToTime as 到期时间
from Userinfo as U,MembershipGrade as M ,Balance as B
where U.LevelNo = M.LevelNo and U.CardNo = B.CardNo and DateDiff(dd,U.OpenCardDate,getdate())<=30   


--通过会员等级查询所有会员信息
select U.CardNo as 卡号,U.MemberState as 会员状态, U.Name as 姓名
,U.PassWord as 密码,U.Wechat as 微信,U.Phone as 手机号,U.Sex as 性别
,U.Birthday as 生日,M.MembersName as 会员等级,B.Balance as 余额,U.Note as 备注
,U.OpenCardDate as 开卡日期,U.DueToTime as 到期时间
from Userinfo as U,MembershipGrade as M ,Balance as B
where U.LevelNo = M.LevelNo and U.CardNo = B.CardNo and U.LevelNo = 3

--通过余额查询所有会员信息
select U.CardNo as 卡号,U.MemberState as 会员状态, U.Name as 姓名
,U.PassWord as 密码,U.Wechat as 微信,U.Phone as 手机号,U.Sex as 性别
,U.Birthday as 生日,M.MembersName as 会员等级,B.Balance as 余额,U.Note as 备注
,U.OpenCardDate as 开卡日期,U.DueToTime as 到期时间
from Userinfo as U,MembershipGrade as M ,Balance as B
where U.LevelNo = M.LevelNo and U.CardNo = B.CardNo and B.Balance >=0
-----------------------------------------------------------------------------------------

---------------------------------消费统计------------------------------------------------

--查询订单表
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo


--查询订单表通过卡号
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo and o.CardNo = '0001'


--查询订单表通过姓名
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo and o.Name = '林先生'

--查询订单表通过日期30天以内
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo and DATEDIFF(DD,o.DateConsumption,GETDATE())<=30

--查询订单表通过消费金额大于300查询
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo and o.ConsumptionMoney >=300

--查询订单表通过消费类型
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo and ct.TypeName = '染发'


--查询订单表通过支付方式
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo and M.TypeName = '会员卡'

--查询订单表通过消费方式
select o.CardNo as 卡号,o.Name as 姓名,cp.TypeName as 消费方式
,M.TypeName as 支付方式,ct.TypeName as 消费类型
,o.ConsumptionMoney as 消费金额,o.ActualMoney as 实收金额,o.DateConsumption as 消费日期,o.Note as 备注
from Orders as o,ConsumptionPatterns as cp,MethodPayment as M,ConsumptionType as ct
where o.ConsumptionPatternsNo = cp.ConsumptionPatternsNo and o.MethodPaymentNo = M.MethodPaymentNo
and o.ConsumptionTypeNo  = ct.ConsumptionTypeNo and cp.TypeName = '卡号'



-----------------------------------------------------------------------------------------


---------------------------------充值统计------------------------------------------------
--查询充值记录表
select R.CardNo as 卡号 ,R.Name as 姓名,R.Credit as 充值金额,R.ActualMoney as 实收金额,R.CreDate as 充值日期,M.TypeName as 支付方式
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo

--查询充值记录表通过卡号
select R.CardNo as 卡号 ,R.Name as 姓名,R.Credit as 充值金额,R.ActualMoney as 实收金额,R.CreDate as 充值日期,M.TypeName as 支付方式
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo and R.CardNo = '0001'


--查询充值记录表通过卡号
select R.CardNo as 卡号 ,R.Name as 姓名,R.Credit as 充值金额,R.ActualMoney as 实收金额,R.CreDate as 充值日期,M.TypeName as 支付方式
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo and R.Name = '林先生'


--查询充值记录表通过充值金额
select R.CardNo as 卡号 ,R.Name as 姓名,R.Credit as 充值金额,R.ActualMoney as 实收金额,R.CreDate as 充值日期,M.TypeName as 支付方式
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo and R.Credit >=300

--查询充值记录表通过支付方式
select R.CardNo as 卡号 ,R.Name as 姓名,R.Credit as 充值金额,R.ActualMoney as 实收金额,R.CreDate as 充值日期,M.TypeName as 支付方式
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo and M.TypeName = '微信'

--查询充值记录表通过充值日期30天内
select R.CardNo as 卡号 ,R.Name as 姓名,R.Credit as 充值金额,R.ActualMoney as 实收金额,R.CreDate as 充值日期,M.TypeName as 支付方式
from Recharge as R,MethodPayment as M
where R.MethodPaymentNo = M.MethodPaymentNo and DATEDIFF(DD,R.CreDate,GETDATE())<=30;


-----------------------------------------------------------------------------------------



---------------------------------短信统计------------------------------------------------

select MessageTypeNo,TypeName from SmsType

--添加短信类型
insert SmsType
values ('开卡'),('充值'),('消费'),('多久没来消费提醒')


--添加短信记录
insert Sms
values ('0001','林先生','18559440650',GETDATE(),
'恭喜您成为我们的高级会员，您的卡号是0001，赠送您1次拉直,卡上余额为800.00元',1)


select s.CardNo,s.Name,s.Phone,s.InDate,s.MessageText,st.TypeName
from sms as s,SmsType as st
where s.MessageTypeNo = st.MessageTypeNo and s.MessageTypeNo = 1
--查询所有短信记录
select s.CardNo as 卡号,s.Name as 姓名,s.Phone as 手机号,s.InDate as 发送日期,s.MessageText as 发送内容,st.TypeName as 短信类型
from sms as s,SmsType as st
where s.MessageTypeNo = st.MessageTypeNo

--查询所有短信记录通过姓名
select s.CardNo as 卡号,s.Name as 姓名,s.Phone as 手机号,s.InDate as 发送日期,s.MessageText as 发送内容,st.TypeName as 短信类型
from sms as s,SmsType as st
where s.MessageTypeNo = st.MessageTypeNo and s.Name = '林先生'

--查询所有短信记录通过手机号
select s.CardNo as 卡号,s.Name as 姓名,s.Phone as 手机号,s.InDate as 发送日期,s.MessageText as 发送内容,st.TypeName as 短信类型
from sms as s,SmsType as st
where s.MessageTypeNo = st.MessageTypeNo and s.Phone = '18559440650'

--查询所有短信记录通过发送日期30天内
select s.CardNo as 卡号,s.Name as 姓名,s.Phone as 手机号,s.InDate as 发送日期,s.MessageText as 发送内容,st.TypeName as 短信类型
from sms as s,SmsType as st
where s.MessageTypeNo = st.MessageTypeNo and DATEDIFF(DD,s.InDate,GETDATE())<=30


--查询所有短信记录通过发送类型
select s.CardNo as 卡号,s.Name as 姓名,s.Phone as 手机号,s.InDate as 发送日期,s.MessageText as 发送内容,st.TypeName as 短信类型
from sms as s,SmsType as st
where s.MessageTypeNo = st.MessageTypeNo and st.TypeName = '开卡'

-----------------------------------------------------------------------------------------


---------------------------------消费类型设置------------------------------------------------


--查询消费类型表
select ConsumptionTypeNo as 消费类型序号,TypeName as 消费类型名
,Price as 价格,Enable as 是否启用,DueToTime as 到期时间
from ConsumptionType

select  TypeName 
,Price ,Enable ,DueToTime 
from ConsumptionType
where TypeName = '染发'

update ConsumptionType 
set TypeName = '染发',Price=12,Enable='启用',DueToTime='2060-01-01'
where TypeName = '染发'

--添加消费类型
insert ConsumptionType
values ('理发','25',1,'2020-10-20')
		,('洗头','50',1,'2020-10-20')
		,('染发','300',1,'2020-10-20')

-----------------------------------------------------------------------------------------

---------------------------------会员等级设置------------------------------------------------

--查询会员等级表
select LevelNo as 会员等级序号,MembersName as 会员等级名称,money as 价格
,Discount as 折扣,Enable as 是否启用,DueToTime as 到期时间
from MembershipGrade

delete from MembershipGrade

--添加会员等级
insert MembershipGrade
values('普通会员',200,0.9,1,'2020-12-12')
,('中级会员',400,0.8,1,'2020-12-12')
,('高级会员',800,0.7,1,'2020-12-12')

update MembershipGrade
set MembersName = '会员',money = 300,Discount=0.1,Enable = '禁用',DueToTime = '2020-12-12'
where MembersName = '会员' 

select LevelNo,MembersName ,money
,Discount,Enable,DueToTime
from MembershipGrade

-----------------------------------------------------------------------------------------

----------------------------------消费/支付方式设置----------------------------------------

--添加消费方式
insert ConsumptionPatterns
values ('微信'),('手机'),('卡号'),('普通消费')


--添加支付方式
insert MethodPayment
values ('微信'),('支付宝'),('会员卡')

select TypeName
from MethodPayment

select TypeName
from ConsumptionPatterns

select c.TypeName,m.TypeName
from ConsumptionPatterns as C,MethodPayment as M

-----------------------------------------------------------------------------------------
insert dbo.QueryMode
values ('微信号'),('手机号'),('姓名'),('卡号')

select QueryNo,QueryName from QueryMode

update UserInfo 
set MemberState = 0 
where DueToTime>'2000-01-01' and  DATEDIFF(DD,GETDATE(),DueToTime)<=0 and  MemberState = 1


