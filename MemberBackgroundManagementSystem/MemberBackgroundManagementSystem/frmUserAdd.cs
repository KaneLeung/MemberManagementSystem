﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MemberManagementSystem
{
    public partial class frmUserAdd : Form
    {
        public frmUserAdd()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 下拉列表框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 下拉列表框方法
        /// </summary>
        public void Pulldown() {
            this.comboBox1.Items.Add("请选择");
            this.comboBox1.SelectedIndex = 0;
            this.comboBox1.Items.Add("正常");
            this.comboBox1.Items.Add("锁定");
        }
        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form2_Load(object sender, EventArgs e)
        {
            Pulldown();
            GetLevel();
        }

        public void GetLevel()
        {
            conn = new SqlConnection(str);
            try
            {
                DataSet ds = new DataSet();
                string sql = string.Format("  select* from  [MemberUserDB].[dbo].[Level]");
                SqlDataAdapter sda = new SqlDataAdapter(sql,conn);
                sda.Fill(ds,"Level");
                this.comboBox2.DataSource = ds.Tables["Level"];
                this.comboBox2.DisplayMember = "LevelName";
                this.comboBox2.ValueMember = "LevelNo";
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 单击添加事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (NotNull())  //验证成功则操作添加数据
            {
                Add();
            }
        }
        string str = "Data Source=10.1.233.6;Initial Catalog=MemberUserDB;Persist Security Info=True;User ID=sa;Password=king520.";
        SqlConnection conn = null;
        string UserName ;
        string Address ;
        public bool IsEmpty() {
            if (this.textBox3.Text.Trim().Equals(string.Empty))
            {
                UserName = null;
                return false;
            }
            else if (this.textBox4.Text.Trim().Equals(string.Empty))
            {
                Address = null;
                return false;
            }
            else {
                return true;
            }
        }
        /// <summary>
        /// 添加数据方法
        /// </summary>
        public void Add() {
            conn = new SqlConnection(str);
            DateTime dt = this.dateTimePicker1.Value;
            try
            {
                conn.Open();
                string sql = string.Format(@"insert [MemberUserDB].[dbo].[User] 
                values('{0}','{1}','{2}','{3}',{4},'{5}','{6}')"
                    ,this.textBox1.Text,this.textBox2.Text,this.textBox3.Text,this.textBox4.Text
                    ,this.comboBox2.SelectedValue,dt,this.comboBox1.Text);
                SqlCommand cmd = new SqlCommand(sql, conn);
                int stu = cmd.ExecuteNonQuery();
                if (stu > 0)
                {
                    MessageBox.Show("添加成功");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("添加失败");
                }
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
            finally {
                conn.Close();
            }
        }




        /// <summary>
        /// 非空验证方法
        /// </summary>
        /// <returns></returns>
        public bool NotNull() {
            if (this.textBox1.Text.Equals(string.Empty) && this.textBox2.Text.Equals(string.Empty) )
            {
                MessageBox.Show("请填入用户基本信息");
                return false;
            }
            else if (this.comboBox1.Text.Equals("请选择"))
            {
                MessageBox.Show("请选择用户账号状态");
                return false;
            }
            else {
                return true;
            }
        }



        /// <summary>
        /// 关闭单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Qutry();
            frmUserManage fm = new frmUserManage();
            fm.Bandingshow();
        }



        /// <summary>
        /// 关闭方法
        /// </summary>
        public void Qutry() {
            this.Close();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pTitle_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
