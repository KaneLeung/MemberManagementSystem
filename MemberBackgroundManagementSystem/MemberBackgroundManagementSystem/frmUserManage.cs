﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace MemberManagementSystem
{
    public partial class frmUserManage : Form
    {
        public frmUserManage()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            Bandingshow();

        }
        /// <summary>
        /// 窗体加载是件方法
        /// </summary>
        /// 
        string str = "Data Source=10.1.233.6;Initial Catalog=MemberUserDB;Persist Security Info=True;User ID=sa;Password=king520.";
        SqlConnection conn = null;
        SqlDataAdapter asd = null;
        DataSet ds = null;
        public void Bandingshow()
        {
            conn = new SqlConnection(str);
            ds = new DataSet();
            string sql = @" select q.UserNo,q.PassWord,q.Name,q.Address,le.LevelName,q.DueToDate,q.State
   from [MemberUserDB].[dbo].[User] as q ,Level as le
   where le.LevelNo = q.LevelType ";
            asd = new SqlDataAdapter(sql, conn);
            asd.Fill(ds, "stu");
            this.dgvUserInfo.DataSource = ds.Tables["stu"];
        }





        public int dr;


        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            frmUserAdd fr = new frmUserAdd();
            fr.ShowDialog();

        }



        /// <summary>
        /// 删除单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            delete();//删除方法
            Bandingshow();//单击删除后在次展现数据
        }




        /// <summary>
        /// 删除方法
        /// </summary>
        public void delete()
        {
            conn = new SqlConnection(str);
            try
            {
                conn.Open();
                string sql = string.Format("delete [MemberUserDB].[dbo].[User] where UserNo = '{0}'", this.dgvUserInfo.SelectedRows[0].Cells[0].Value);
                SqlCommand cmd = new SqlCommand(sql, conn);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    MessageBox.Show("删除成功");
                }
                else
                {
                    MessageBox.Show("删除失败");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
        }




        /// <summary>
        /// 右键快捷删除事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            delete();//删除方法
            Bandingshow();//单击删除后在次展现数据
        }



        /// <summary>
        /// 单击锁定事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLock_Click(object sender, EventArgs e)
        {
            Lock_1();
            Bandingshow();
        }


        /// <summary>
        /// 锁定事件方法
        /// </summary>
        public void Lock_1()
        {
            conn = new SqlConnection(str);
            try
            {
                conn.Open();
                string sql = string.Format("update [MemberUserDB].[dbo].[User]  set state = '锁定' where UserNo = '{0}'", this.dgvUserInfo.SelectedRows[0].Cells[0].Value);
                SqlCommand cmd = new SqlCommand(sql, conn);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    MessageBox.Show("修改成功");
                }
                else
                {
                    MessageBox.Show("修改失败，请确认是否选中");
                }
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }

        }


        /// <summary>
        /// 单击恢复正常事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBackToNormal_Click(object sender, EventArgs e)
        {
            PutBack();
            Bandingshow();

        }


        /// <summary>
        /// 判断用户状态是什么
        /// </summary>
        /// <returns></returns>
        public bool assd()
        {
            string an = this.dgvUserInfo.SelectedRows[0].Cells[6].Value.ToString();
            if (an.Equals("锁定"))
            {
                MessageBox.Show("修改");
                return false;
            }
            else
            {

                return true;
            }
        }







        /// <summary>
        /// 恢复正常方法
        /// </summary>
        public void PutBack()
        {
            conn = new SqlConnection(str);
            try
            {
                conn.Open();
                string sql = string.Format("update [MemberUserDB].[dbo].[User]  set state = '正常' where UserNo = '{0}'", this.dgvUserInfo.SelectedRows[0].Cells[0].Value);
                SqlCommand cmd = new SqlCommand(sql, conn);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    MessageBox.Show("修改成功");
                }
                else
                {
                    MessageBox.Show("修改失败，请确认是否选中");
                }

            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
        }



        /// <summary>
        /// 右键锁定快捷单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 锁定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lock_1();
            Bandingshow();
        }


        /// <summary>
        ///右键恢复正常事件 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 恢复正常ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PutBack();
            Bandingshow();
        }

        private void 添加ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
   
        /// <summary>
        /// 跳转到修改窗口方法
        /// </summary>
        public void Modify()
        {
            frmUserUpdate fr = new frmUserUpdate();
            fr.son = this.dgvUserInfo.SelectedRows[0].Cells[0].Value.ToString();
            fr.ShowDialog();
        }

        private void 修改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modify();
            Bandingshow();
        }







    }
}
