﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MemberManagementSystem
{
    public partial class frmLoginLog : Form
    {
        public frmLoginLog()
        {
            InitializeComponent();
        }
        string str = "Data Source=10.1.233.6;Initial Catalog=MemberUserDB;Persist Security Info=True;User ID=sa;Password=king520.";
        SqlConnection conn = null;
        SqlDataAdapter sda = null;
        DataSet ds = null;
        /// <summary>
        /// 获取登录信息
        /// </summary>
        public void GetLoginLogInfo(string querySql)
        {
            conn = new SqlConnection(str);
            try
            {
                ds = new DataSet();
                string sql = string.Format(@"select UserNo,DeviceName,LoginDate,LoginIpAddress
                 from LoginLog {0}", querySql);
                sda = new SqlDataAdapter(sql, conn);
                sda.Fill(ds, "LoginLog");
                this.dgvLoginLog.DataSource = ds.Tables["LoginLog"];
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 获取用户账号
        /// </summary>
        public void GetUserNo()
        {
            conn = new SqlConnection(str);
            try
            {
                ds = new DataSet();
                string sql = string.Format(@"  select UserNo
                FROM [MemberUserDB].[dbo].[User]");
                sda = new SqlDataAdapter(sql, conn);
                sda.Fill(ds, "UserNo");
                DataRow row = ds.Tables["UserNo"].NewRow();
                row[0] = -1;
                row[0] = "全部";
                ds.Tables["UserNo"].Rows.InsertAt(row, 0);
                this.comboBox1.DataSource = ds.Tables["UserNo"];
                this.comboBox1.DisplayMember = "UserNo";
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 获取计算机名
        /// </summary>
        public void GetDeviceName()
        {
            conn = new SqlConnection(str);
            try
            {
                string sql = string.Format("select distinct DeviceName from LoginLog");
                ds = new DataSet();
                sda = new SqlDataAdapter(sql, conn);
                sda.Fill(ds, "DeviceName");
                DataRow row = ds.Tables["DeviceName"].NewRow();
                row[0] = -1;
                row[0] = "全部";
                ds.Tables["DeviceName"].Rows.InsertAt(row, 0);
                this.comboBox2.DataSource = ds.Tables["DeviceName"];
                this.comboBox2.DisplayMember = "DeviceName";
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 获取ip地址
        /// </summary>
        public void GetIpAddress()
        {
            conn = new SqlConnection(str);
            try
            {
                ds = new DataSet();
                string sql = string.Format("select distinct LoginIpAddress from LoginLog ");
                sda = new SqlDataAdapter(sql, conn);
                sda.Fill(ds, "IpAddress");
                DataRow row = ds.Tables["IpAddress"].NewRow();
                row[0] = -1;
                row[0] = "全部";
                ds.Tables["IpAddress"].Rows.InsertAt(row, 0);
                this.comboBox3.DataSource = ds.Tables["IpAddress"];
                this.comboBox3.DisplayMember = "LoginIpAddress";
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void frmLoginLog_Load(object sender, EventArgs e)
        {
            //获取登录信息
            GetLoginLogInfo(null);

            //获取用户账号
            GetUserNo();

            //获取计算机名
            GetDeviceName();

            //获取ip地址
            GetIpAddress();
        }

        public void Filter()
        {
            int i = 0;
            StringBuilder sqlsb = new StringBuilder();
            if (!comboBox1.Text.Trim().Equals("全部") || !comboBox2.Text.Trim().Equals("全部") || !comboBox3.Text.Trim().Equals("全部"))
            {
                sqlsb.AppendFormat(" where ");
            }
            if (!comboBox1.Text.Trim().Equals("全部"))
            {
                sqlsb.AppendFormat("UserNo = '{0}'", comboBox1.Text);
                i++;
            }
            if (!comboBox2.Text.Trim().Equals("全部"))
            {
                if (i==1)
                {
                    sqlsb.Append(" and ");
                }
                sqlsb.AppendFormat("DeviceName = '{0}'", comboBox2.Text);
                i++;
            }
            if (!comboBox3.Text.Trim().Equals("全部"))
            {
                if (i==2)
                {
                    sqlsb.Append(" and ");
                }
                sqlsb.AppendFormat("LoginIpAddress = '{0}'", comboBox3.Text);
            }
            GetLoginLogInfo(sqlsb.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Filter();
        }


    }
}
